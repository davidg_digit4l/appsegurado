import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  //  path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)},
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard] },
  { path: 'misseguros', loadChildren: () => import('./pages/mis-seguros/mis-seguros.module').then(m => m.MisSegurosPageModule) },
  { path: 'miavatar', loadChildren: () => import('./mi-avatar/mi-avatar.module').then(m => m.MiAvatarPageModule), canActivate: [AuthGuard] },
  { path: 'mis-seguros', loadChildren: './pages/mis-seguros/mis-seguros.module#MisSegurosPageModule', canActivate: [AuthGuard] },
  { path: 'login', loadChildren: './pages/auth/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/auth/register/register.module#RegisterPageModule' },
  { path: 'categorias', loadChildren: './pages/categorias/categorias.module#CategoriasPageModule' },
  { path: 'asegurapoliza', loadChildren: './pages/asegurapoliza/asegurapoliza.module#AsegurapolizaPageModule' },
  { path: 'crearseguro', loadChildren: './pages/crearseguro/crearseguro.module#CrearseguroPageModule' },
  { path: 'categoriascotizar', loadChildren: './pages/categoriascotizar/categoriascotizar.module#CategoriascotizarPageModule' , canActivate: [AuthGuard] },
  { path: 'cotizar', loadChildren: './pages/cotizar/cotizar.module#CotizarPageModule' , canActivate: [AuthGuard] },
  { path: 'cotizarformulario', loadChildren: './pages/cotizarformulario/cotizarformulario.module#CotizarformularioPageModule' , canActivate: [AuthGuard] },
  { path: 'aseguradoras', loadChildren: './pages/aseguradoras/aseguradoras.module#AseguradorasPageModule' },
  { path: 'aseguradora', loadChildren: './pages/aseguradora/aseguradora.module#AseguradoraPageModule' },
  { path: 'welcome', loadChildren: './pages/welcome/welcome.module#WelcomePageModule' },
  { path: 'mi-perfil', loadChildren: './pages/mi-perfil/mi-perfil.module#MiPerfilPageModule' },
  { path: 'detalle', loadChildren: './pages/detalle/detalle.module#DetallePageModule' },
  { path: 'ayuda', loadChildren: './pages/ayuda/ayuda.module#AyudaPageModule' },
  { path: 'chat', loadChildren: './pages/chat/chat.module#ChatPageModule' },
  { path: 'llamada', loadChildren: './pages/llamada/llamada.module#LlamadaPageModule' },
  { path: 'preguntas', loadChildren: './pages/preguntas/preguntas.module#PreguntasPageModule' },
  { path: 'automoviles', loadChildren: './form/automoviles/automoviles.module#AutomovilesPageModule' , canActivate: [AuthGuard] },
  { path: 'soat', loadChildren: './form/soat/soat.module#SoatPageModule' , canActivate: [AuthGuard] },
  { path: 'categoriaproducto', loadChildren: './pages/categoriaproducto/categoriaproducto.module#CategoriaproductoPageModule' },
  { path: 'sarlaft', loadChildren: './form/sarlaft/sarlaft.module#SarlaftPageModule' , canActivate: [AuthGuard] },
  { path: 'automoviles-cotizacion', loadChildren: './formcotizacion/automoviles-cotizacion/automoviles-cotizacion.module#AutomovilesCotizacionPageModule' , canActivate: [AuthGuard] },
  { path: 'soat-cotizacion', loadChildren: './formcotizacion/soat-cotizacion/soat-cotizacion.module#SoatCotizacionPageModule' , canActivate: [AuthGuard] },
  { path: 'bicicletas-cotizacion', loadChildren: './formcotizacion/bicicletas-cotizacion/bicicletas-cotizacion.module#BicicletasCotizacionPageModule', canActivate: [AuthGuard] },
  { path: 'bicicletas', loadChildren: './form/bicicletas/bicicletas.module#BicicletasPageModule' , canActivate: [AuthGuard] },
  { path: 'salud', loadChildren: './form/salud/salud.module#SaludPageModule', canActivate: [AuthGuard] },
  { path: 'salud-cotizacion', loadChildren: './formcotizacion/salud-cotizacion/salud-cotizacion.module#SaludCotizacionPageModule', canActivate: [AuthGuard] },
  { path: 'mis-cotizaciones', loadChildren: './pages/mis-cotizaciones/mis-cotizaciones.module#MisCotizacionesPageModule', canActivate: [AuthGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: 'mi-avatar', loadChildren: './mi-avatar/mi-avatar.module#MiAvatarPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
