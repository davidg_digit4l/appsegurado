import { Component, OnInit } from '@angular/core';
import { EndpointService } from '../services/endpoint.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-mi-avatar',
  templateUrl: './mi-avatar.page.html',
  styleUrls: ['./mi-avatar.page.scss'],
})
export class MiAvatarPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400,
  };

  id_usuario: any;

  activapiel = true;
  activapelo = false;
  activaojos = false;
  activaropa = false;

  ojos: any ;
  pelo: any ;
  piel: any ;
  ropa: any ;


  constructor(public _endpoint: EndpointService, public _auth: AuthService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    
    this._auth.user().subscribe(
      (user: any) => {
        this.cargarAvatar(user.id);
      
      this.id_usuario = user;
    }
    );
  }

  //funciones de cambio de opcion del avatar

  cambiarpiel() {
    this.activapiel = true;
    this.activapelo = false;
    this.activaojos = false;
    this.activaropa = false;
  }

  cambiarpelo() {
    this.activapiel = false;
    this.activapelo = true;
    this.activaojos = false;
    this.activaropa = false;
  }

  cambiarojos() {
    this.activapiel = false;
    this.activapelo = false;
    this.activaojos = true;
    this.activaropa = false;
  }

  cambiarropa() {
    this.activapiel = false;
    this.activapelo = false;
    this.activaojos = false;
    this.activaropa = true;
  }

  cargarAvatar(id) {
    
    this._endpoint._CargarAvatar(id).subscribe(
      (res: any) => {
        this.ojos = res[0].ojo;
        this.pelo = res[0].pelo;
        this.piel = res[0].piel;
        this.ropa = res[0].ropa;
      }
    );
  }

  actualizarAvatar() {
    let data = {
      id: this.id_usuario.id,
      piel: this.piel,
      ojo: this.ojos,
      ropa: this.ropa,
      pelo: this.pelo
    };
    this._endpoint._UpdateAvatar(data).subscribe(
      (resp: any) => {
        console.log(resp);
        
      }
    );
  }

  asignarPelo(pelo) {
    this.pelo = pelo;    
  }

  asignarPiel(piel) {
    this.piel = piel;    
  }
  asignarRopa(ropa) {
    this.ropa = ropa;    
  }
  asignarOjo(ojo) {
    this.ojos = ojo;    
  }

}
