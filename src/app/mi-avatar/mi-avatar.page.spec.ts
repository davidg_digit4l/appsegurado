import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiAvatarPage } from './mi-avatar.page';

describe('MiAvatarPage', () => {
  let component: MiAvatarPage;
  let fixture: ComponentFixture<MiAvatarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiAvatarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiAvatarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
