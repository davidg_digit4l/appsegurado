import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {


  constructor(
    private http: HttpClient,
    private env: EnvService,
  ) {}

  // categorias
  _getCategorias() {
    return this.http.get(this.env.API_URL + 'categorias');
  }

  _GetFiltroSeguroCategoria(idseguro) {
    return this.http.get(this.env.API_URL + 'categoria/' + idseguro);
  }

  _GetFiltroCategorias(idcategoria) {
    return this.http.get(this.env.API_URL + 'producto/' + idcategoria);
  }

  _ListaSeguro() {
    return this.http.get(this.env.API_URL + 'seguros');
  }

  _GetSeguros(iduser) {
    return this.http.get(this.env.API_URL + 'seguros/' + iduser);
  }

  _GetSegurosDetalle(idseguro) {
    return this.http.get(this.env.API_URL + 'seguros/detalle/' + idseguro);
  }

  _GetSegurosDetalleInterna(idseguro) {
    return this.http.get(this.env.API_URL + 'detalle/aseguradora/' + idseguro);
  }

  _GetProductoseguro(idproducto) {
    return this.http.get(this.env.API_URL + 'seguro/' + idproducto);
  }

  //cotizar poliza
  _PostCotizacionPolizas(data){
    return this.http.post(this.env.API_URL + 'cotizacionpolizas', data);
  }

  //form sarlaft
  _PostSarlaft(data){
    return this.http.post(this.env.API_URL + 'sarlaft', data);
  }


  _GetCotizar(user_id){
    return this.http.get(this.env.API_URL + 'cotizacionappuser/' + user_id );
  }

  /*formularios de cotizaciones y  seguros*/

  _PostCrearSeguro(data) {
    return this.http.post(this.env.API_URL + 'automoviles', data);
  }

  _PostCrearSoat(data) {
    return this.http.post(this.env.API_URL + 'soat', data);
  }

  _PostCrearBicicletas(data) {
    return this.http.post(this.env.API_URL + 'bicicletas', data);
  }

  _PostCrearSalud(data) {
    return this.http.post(this.env.API_URL + 'salud', data);
  }

  _PostCrearEnfermedadesGraves(data) {
    return this.http.post(this.env.API_URL + 'enfermedadesGraves', data);
  }

  _PostCrearEducativo(data) {
    return this.http.post(this.env.API_URL + 'educativo', data);
  }

  _PostCrearExequial(data) {
    return this.http.post(this.env.API_URL + 'exequial', data);
  }

  _PostCrearVida(data) {
    return this.http.post(this.env.API_URL + 'vida', data);
  }

  _PostCrearAccidentesPersonales(data) {
    return this.http.post(this.env.API_URL + 'accidentesPersonales', data);
  }

  _PostCrearDesempleo(data) {
    return this.http.post(this.env.API_URL + 'desempleo', data);
  }

  _PostCrearHogar(data) {
    return this.http.post(this.env.API_URL + 'hogar', data);
  }

  _PostCrearMascotas(data) {
    return this.http.post(this.env.API_URL + 'mascotas', data);
  }

  _PostCrearRc(data) {
    return this.http.post(this.env.API_URL + 'rc', data);
  }

  _PostCrearArriendos(data) {
    return this.http.post(this.env.API_URL + 'arriendos', data);
  }

  _PostCrearViajes(data) {
    return this.http.post(this.env.API_URL + 'viajes', data);
  }

  _PostCrearMotos(data) {
    return this.http.post(this.env.API_URL + 'motos', data);
  }
  
  _PostCrearElectronicos(data) {
    return this.http.post(this.env.API_URL + 'electronicos', data);
  }

  //endpoints de personalizacion de avatar

  _CargarAvatar(id_user) {
    return this.http.get(this.env.API_URL + 'cargaravatar/' + id_user);
  }

  _UpdateAvatar(data) {
    return this.http.put( this.env.API_URL + 'actualizaravatar', data );
  }


}
