import { Component } from '@angular/core';
import { Platform, NavController, MenuController, AlertController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { AlertService } from './services/alert.service';
import { PushService } from './services/push.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
   },
   {
     title: 'Mi Perfil',
      url: '/mi-perfil',
      icon: 'ios-body'
    },

    {
      title: 'Mis Seguros',
      url: '/misseguros',
      icon: 'md-albums'
    },

    {
      title: 'Mis Cotizaciones',
      url: '/mis-cotizaciones',
      icon: 'md-pricetags'
   },
    {
      title: 'Personaliza tu avatar',
      url: '/miavatar',
      icon: 'md-contact'
   },

    {
      title: 'Ayuda',
      url: '/ayuda',
      icon: 'md-chatboxes'
   }

  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private menu: MenuController,
    private pushService: PushService,
    private screenOrientation: ScreenOrientation,
    public loadingController: LoadingController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      // this.splashScreen.hide();
      console.log(this.screenOrientation.type); // logs the current orientation, example: 'landscape'
      this.authService.getToken();
      this.menu.enable(false, 'first');
      this.pushService.setupPush();
      // set to landscape
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      // allow user rotate
      // this.screenOrientation.unlock();
    });
  }
  // When Logout Button is pressed 
  logout() {
    this.presentLoadingWithOptions();
    this.authService.logout().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        this.alertService.presentToast('Esperamos que vuelvas pronto');
      },
      error => {
        console.log(error);
      },
      () => {
        this.navCtrl.navigateRoot('/login');
        this.menu.enable(false, 'first');
        this.menu.close('first');
      }
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2000,
      message: 'Espera un momento...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  } 
}
