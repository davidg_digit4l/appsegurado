export class User {
    id: number;
    name: String;
    cedula:  number;
    genero: String;
    onesignal_id: String;
    telefono: number; 
    email: String;
    password: String;
    sarlaft: any;
}