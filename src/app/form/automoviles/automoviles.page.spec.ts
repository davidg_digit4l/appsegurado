import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomovilesPage } from './automoviles.page';

describe('AutomovilesPage', () => {
  let component: AutomovilesPage;
  let fixture: ComponentFixture<AutomovilesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomovilesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomovilesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
