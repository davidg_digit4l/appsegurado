import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoatPage } from './soat.page';

describe('SoatPage', () => {
  let component: SoatPage;
  let fixture: ComponentFixture<SoatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
