import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicicletasPage } from './bicicletas.page';

describe('BicicletasPage', () => {
  let component: BicicletasPage;
  let fixture: ComponentFixture<BicicletasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicicletasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicicletasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
