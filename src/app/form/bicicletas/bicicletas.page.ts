import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import * as moment from "moment";
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-bicicletas',
  templateUrl: './bicicletas.page.html',
  styleUrls: ['./bicicletas.page.scss'],
})
export class BicicletasPage implements OnInit {

  listaseguro: any;
  id: any;
  titulo: any;
  nombre: any;
  apellido: any;
  cedula: any;
  nacimiento: any;
  genero: any;
  vigencia: any;
  aseguradora: any;
  valor: any;
  marca: any;
  referencia: any;
  documento: any;
  estado:any;
  base64textString: string;
  user: User;
  tipo: string;


  constructor(
    public api: EndpointService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private alertService: AlertService,
    private navCtrl: NavController,
    public loadingController: LoadingController,
  ) {}

  ngOnInit() {
    this.ListarSeguros();
  }


  ListarSeguros() {
    this.api._ListaSeguro().subscribe(response => {
      this.listaseguro = response;
      console.log('filtro por aseguradora', this.listaseguro);
    });
  }

  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload =this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
   this.base64textString = btoa(binaryString);
   console.log(this.base64textString);
  }


 
  ActualizarPerfil() {
    this.presentLoadingWithOptions();
    this.authService.user().subscribe(
      user => {
        this.user = user;
        const datos = {
          id: this.user.id,
          titulo: this.titulo,
          nombre: this.nombre,
          apellido: this.apellido,
          cedula: this.cedula,
          nacimiento: moment(this.nacimiento).format('YYYY-MM-DD'),
          genero: this.genero,
          vigencia: moment(this.vigencia).format('YYYY-MM-DD'),
          aseguradora: this.aseguradora,
          valor: this.valor,
          marca: this.marca,
          referencia: this.referencia,
          estado: 'aprobado',
          documento: 'data:application/pdf;base64,' + this.base64textString
        };
    
        this.api._PostCrearBicicletas(datos).subscribe(
          request => {
            this.alertService.presentToast("hemos creado tu seguro");
            this.presentLoadingWithOptions();
            this.navCtrl.navigateRoot('/home');
          },error => {
            console.log(error);
          });
      }
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2000,
      message: 'Espera un momento...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  } 


}
