import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { LoadingController, NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-sarlaft',
  templateUrl: './sarlaft.page.html',
  styleUrls: ['./sarlaft.page.scss'],
})
export class SarlaftPage implements OnInit {
  user: any;
  nacimiento: any;
  nacionalidad: any;
  dirrecion_recidencia: any;
  ciudad: any;
  profesion: any;
  actividad: any;
  empresa: any;
  cargo: any;
  telefono_comercial: any;
  direcion_comercial: any;
  ciudad_trabajo: any;
  recursos_publicos: any;
  reconocimiento_publico: any;
  poder_publico: any;

  constructor(
    public api: EndpointService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    private router: Router,
  ) {
    this.user = this.activatedRoute.snapshot.paramMap.get('user');
  }

  ngOnInit() {
    console.log(this.user);
  }

  EnviarSarlaft() {
    this.presentLoadingWithOptions();
    const datos = {
      user: this.user,
      nacimiento:  this.nacimiento,
      nacionalidad:  this.nacionalidad,
      dirrecion_recidencia: this.dirrecion_recidencia,
      ciudad:  this.ciudad,
      profesion:  this.profesion,
      actividad: this.actividad,
      empresa:  this.empresa,
      cargo:  this.cargo,
      telefono_comercial:  this.telefono_comercial,
      direcion_comercial: this.direcion_comercial,
      ciudad_trabajo: this.ciudad_trabajo,
      recursos_publicos:  this.recursos_publicos,
      reconocimiento_publico:  this.reconocimiento_publico,
      poder_publico: this.poder_publico
    };

    
    this.api._PostSarlaft(datos).subscribe(
     request => {
       
      console.log('guardado');
        this.alertService.presentToast("Gracias por terminar el registro del sarlaft ya puedes realizar cotizaciones");
      this.presentLoadingWithOptions();
        this.navCtrl.navigateRoot('/home');
      } , error => {
        console.log(error);
   });

  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2000,
      message: 'Espera un momento...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  } 


}
