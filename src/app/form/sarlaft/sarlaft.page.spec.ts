import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SarlaftPage } from './sarlaft.page';

describe('SarlaftPage', () => {
  let component: SarlaftPage;
  let fixture: ComponentFixture<SarlaftPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SarlaftPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SarlaftPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
