import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BicicletasCotizacionPage } from './bicicletas-cotizacion.page';

const routes: Routes = [
  {
    path: '',
    component: BicicletasCotizacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BicicletasCotizacionPage]
})
export class BicicletasCotizacionPageModule {}
