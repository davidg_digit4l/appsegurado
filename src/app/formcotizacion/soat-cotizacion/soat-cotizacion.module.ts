import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SoatCotizacionPage } from './soat-cotizacion.page';

const routes: Routes = [
  {
    path: '',
    component: SoatCotizacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SoatCotizacionPage]
})
export class SoatCotizacionPageModule {}
