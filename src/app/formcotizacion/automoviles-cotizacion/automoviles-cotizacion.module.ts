import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AutomovilesCotizacionPage } from './automoviles-cotizacion.page';

const routes: Routes = [
  {
    path: '',
    component: AutomovilesCotizacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AutomovilesCotizacionPage]
})
export class AutomovilesCotizacionPageModule {}
