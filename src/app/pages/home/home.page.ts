import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController,AlertController  } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  user: User;
  user_id: any;
  pelo: any;
  ojos: any;
  piel: any;
  ropa: any;


    constructor(
      private menu: MenuController, 
      private authService: AuthService,
      private router: Router,
      public alertController: AlertController,
      private alertService: AlertService,
      private activatedRoute: ActivatedRoute
      ) { 
      this.menu.enable(true);
      
    }
    
    ngOnInit() {
      
    }
    
    ionViewWillEnter() {
      this.authService.user().subscribe(
        user => {
          console.log('id usuario', user.id);
          this.cargarAvatar(user.id);
        
        this.user = user;
      }
    );
  }

  cargarAvatar(user_id) {
    this.authService.loadAvatar(user_id).subscribe(
      (res: any) => {
        console.log('avatar',res[0]);
        this.ojos = res[0].ojo;
        this.pelo = res[0].pelo;
        this.piel = res[0].piel;
        this.ropa = res[0].ropa;
      }
    );
  }


  VerificarSarlaft() {
    this.authService.user().subscribe(
      user => {
        this.user = user;

        if( this.user.sarlaft == 'no'){
          // tslint:disable-next-line: max-line-length
          this.alertService.presentToast("Para poder cotizar es necesario llenar una única vez el siguiente formulario, completalo para continuar, Gracias. :)");
        setTimeout(() => {
          this.router.navigate(['sarlaft', {user:this.user.id}]);
        }, 4000);  
        }
      
        if(this.user.sarlaft == 'si'){
          this.router.navigate(['cotizar']);
        }
        
      });
  }

}
