import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisSegurosPage } from './mis-seguros.page';

describe('MisSegurosPage', () => {
  let component: MisSegurosPage;
  let fixture: ComponentFixture<MisSegurosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisSegurosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisSegurosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
