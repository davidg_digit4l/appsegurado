import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
  selector: 'app-mis-seguros',
  templateUrl: './mis-seguros.page.html',
  styleUrls: ['./mis-seguros.page.scss'],
})
export class MisSegurosPage implements OnInit {

  user: User;
  seguros: Object;

  constructor(
    private router: Router,
    public api: EndpointService,
    private authService: AuthService,
    ) { }

  ngOnInit() {
    this.MisSeguros();
  }

  MisSeguros() {
    this.authService.user().subscribe(
      user => {
        this.user = user;
        console.log(this.user);
        this.api._GetSeguros(this.user.id).subscribe(data => {
          this.seguros = data;
          console.log(this.seguros);
        })
      }
    );
  }

  detalle(id) {
    console.log('id',id);
    this.router.navigate(['detalle', { id:id }]);
  }

}
