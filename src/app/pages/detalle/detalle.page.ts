import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  paramId: any;
  info: any;

  constructor(
    public api: EndpointService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {
    this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.detalle();
  }

  detalle(){
    console.log('iddetalle', this.paramId);
    this.api._GetSegurosDetalle(this.paramId).subscribe(data => {
      this.info = data;
      console.log(this.info);
    })
  }

}
