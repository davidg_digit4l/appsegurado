import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizarformularioPage } from './cotizarformulario.page';

describe('CotizarformularioPage', () => {
  let component: CotizarformularioPage;
  let fixture: ComponentFixture<CotizarformularioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizarformularioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizarformularioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
