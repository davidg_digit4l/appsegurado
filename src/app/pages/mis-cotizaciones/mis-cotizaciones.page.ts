import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingController } from '@ionic/angular';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-mis-cotizaciones',
  templateUrl: './mis-cotizaciones.page.html',
  styleUrls: ['./mis-cotizaciones.page.scss'],
})
export class MisCotizacionesPage implements OnInit {

  cotizaciones: any;
  user: User;

  constructor(
    public api: EndpointService,
    private authService: AuthService,
    private alertService: AlertService,
    public loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.GetCotizaciones();
  }

  GetCotizaciones(){
    this.authService.user().subscribe(
      user => {
        this.user = user;
        console.log('user',this.user);
        this.api._GetCotizar(this.user.id).subscribe(data => {
          this.cotizaciones = data;
          console.log('cotizaciones',this.cotizaciones);
        })
      }
    );
    
  }

}
