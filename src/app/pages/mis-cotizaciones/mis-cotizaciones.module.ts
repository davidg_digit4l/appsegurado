import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisCotizacionesPage } from './mis-cotizaciones.page';

const routes: Routes = [
  {
    path: '',
    component: MisCotizacionesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MisCotizacionesPage]
})
export class MisCotizacionesPageModule {}
