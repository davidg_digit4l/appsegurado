import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
  selector: 'app-llamada',
  templateUrl: './llamada.page.html',
  styleUrls: ['./llamada.page.scss'],
})
export class LlamadaPage implements OnInit {

  user: User;
  seguros: Object;
  paramId: any;

  constructor(
    private router: Router,
    public api: EndpointService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,

    ) {this.paramId = this.activatedRoute.snapshot.paramMap.get('id'); }

  ngOnInit() {
    this.MisSeguros();
  }

  MisSeguros() {

    this.api._GetSegurosDetalleInterna(this.paramId).subscribe(data => {
      this.seguros = data;
      console.log(this.seguros);
    });
  }


}
