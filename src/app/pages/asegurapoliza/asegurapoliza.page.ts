import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-asegurapoliza',
  templateUrl: './asegurapoliza.page.html',
  styleUrls: ['./asegurapoliza.page.scss'],
})
export class AsegurapolizaPage implements OnInit {

  paramId: any;
  seguros: any;

  constructor(
    public api: EndpointService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    this.ListarSeguros();
  }

  ListarSeguros() {
    this.api._GetFiltroCategorias(this.paramId).subscribe(response => {
      this.seguros = response;
      console.log('filtro por aseguradora', this.seguros);
    });
  }

  detalle(id) {
    console.log('id producto', id);
    if(id == 1) {
      this.router.navigate(['automoviles', {id:id}]);
    }

    if(id == 2) {
      this.router.navigate(['soat', {id:id}]);
    }

    if(id == 3) {
      this.router.navigate(['bicicletas', {id:id}]);
    }

    if(id == 4) {
      this.router.navigate(['salud', {id:id}]);
    }

    // this.router.navigate(['asegurapoliza', {id:id}]);
  }



}
