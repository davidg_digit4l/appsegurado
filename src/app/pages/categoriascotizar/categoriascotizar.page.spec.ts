import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriascotizarPage } from './categoriascotizar.page';

describe('CategoriascotizarPage', () => {
  let component: CategoriascotizarPage;
  let fixture: ComponentFixture<CategoriascotizarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriascotizarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriascotizarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
