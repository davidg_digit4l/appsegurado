import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { LoadingController, NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-categoriascotizar',
  templateUrl: './categoriascotizar.page.html',
  styleUrls: ['./categoriascotizar.page.scss'],
})
export class CategoriascotizarPage implements OnInit {
  
  paramId: any;
  selectedArray: any = [];
  testList: any = [];
  cotizacion:any;
  user: User;
  //checked: any ;

  constructor(
    public api: EndpointService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    private router: Router,
    
    ) {
      this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
      this.testList.checked = false;
   }

  //  testList: any = [
  //    {testID: 1, testName: " test1", checked: false},
  //    {testID: 2, testName: " test2", checked: false},
  //    {testID: 3, testName: "dgdfgd", checked: false},
  //    {testID: 4, testName: "UricAcid", checked: false}
  // ]

  ngOnInit() {
    this.checkAll();
    //this.VerificarSarlaft();
  }

// checkAll(){
//     for(let i =0; i <= this.testList.length; i++) {
//       this.testList[i].checked = false;
//     }
//    console.log(this.testList);
//   }

  checkAll() {
    this.api. _GetProductoseguro(this.paramId).subscribe(response => {
      this.testList = response;
      for(let i = 0; i <= this.testList.length; i++) {
        this.testList[i].checked = false;
      }
      console.log('arreglo',this.testList);
    });
  } 
  
  selectMember(data){
   if (data.checked == false) {
      this.selectedArray.push(data);
    } else {
     let newArray = this.selectedArray.filter(function(el) {
       return el.seguros_id !== data.seguros_id;
    });
     this.selectedArray = newArray;
   }
   console.log(this.selectedArray);
  }

  VerificarSarlaft(){
    this.presentLoadingWithOptions();
    this.authService.user().subscribe(
      user => {
        this.user = user;
        let cotizacion = {
          id_tips: this.paramId,
          user_id: this.user.id,
          id_seguro: this.selectedArray,
        }; 
        if(this.user.sarlaft == 'si'){
          this.api._PostCotizacionPolizas(cotizacion).subscribe(data => {
            this.cotizacion = data;
            this.presentLoadingWithOptions();
            if(this.paramId == 1) {
              this.presentLoadingWithOptions();
              this.router.navigate(['automoviles-cotizacion', {id: this.paramId}]);
            }
        
            if(this.paramId == 2) {
              this.presentLoadingWithOptions();
              this.router.navigate(['soat-cotizacion', {id: this.paramId}]);
            }

            if(this.paramId == 3) {
              this.presentLoadingWithOptions();
              this.router.navigate(['bicicletas-cotizacion', {id: this.paramId}]);
            }

            if(this.paramId == 4) {
              this.presentLoadingWithOptions();
              this.router.navigate(['salud-cotizacion', {id: this.paramId}]);
            }
            
          },error => {
            console.log(error);
          });
        }
      }
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2000,
      message: 'Espera un momento...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  } 
}
