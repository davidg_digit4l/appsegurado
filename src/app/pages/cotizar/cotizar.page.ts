import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cotizar',
  templateUrl: './cotizar.page.html',
  styleUrls: ['./cotizar.page.scss'],
})
export class CotizarPage implements OnInit {

  categorias: any;
  categoria: any;
  producto: any;
  paramId: any;

  constructor( 
    public api: EndpointService,
    private router: Router
    ) {
      
    }

  ngOnInit() {
    this.ListarCategorias();
  }

  ListarCategorias() {
    this.api._getCategorias().subscribe(
      response => {
        this.categorias = response;
        console.log('lista de categorias', this.categorias);
      });
  }

  detalle(id) {
    console.log(id);
    this.router.navigate(['categoriaproducto', {id:id}]);
  }

}
