import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
  selector: 'app-aseguradoras',
  templateUrl: './aseguradoras.page.html',
  styleUrls: ['./aseguradoras.page.scss'],
})
export class AseguradorasPage implements OnInit {
  seguros: any;

  constructor(
    private router: Router,
    public api: EndpointService,
    private authService: AuthService,
    ) { }

  ngOnInit() {
    this.MisSeguros();
  }

  MisSeguros() {
    this.api._ListaSeguro().subscribe(data => {
      this.seguros = data;
      console.log(this.seguros);
    })
  }


  detalle(id){
    console.log(id);
    this.router.navigate(['llamada', {id:id}]);
  }
  

}
