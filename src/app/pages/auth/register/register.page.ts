import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { PushService } from 'src/app/services/push.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(private modalController: ModalController,
              private authService: AuthService,
              private navCtrl: NavController,
              private alertService: AlertService,
              // tslint:disable-next-line: no-shadowed-variable
              public PushService: PushService,
              public loadingController: LoadingController,
  ) { }

  ngOnInit() {
    console.log(this.PushService.UserId);
  }


  register(form: NgForm) {
    this.presentLoadingWithOptions();
      // this.authService.register(/*this.PushService.UserId,*/ form.value.name, form.value.email, form.value.password).subscribe(
      // tslint:disable-next-line: max-line-length
      this.authService.register(this.PushService.UserId, form.value.name, form.value.cedula, form.value.genero,  form.value.telefono,  form.value.email, form.value.password).subscribe(
      data => {
        this.authService.login(form.value.email, form.value.password).subscribe(
          data => {
            this.presentLoadingWithOptions();
          },
          error => {
            console.log(error);
          },
          () => {

            this.navCtrl.navigateRoot('/welcome');
          }
        );
        this.alertService.presentToast('Bienvenido appsegurado');
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2000,
      message: 'Espera un momento...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  } 

}
