import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categoriaproducto',
  templateUrl: './categoriaproducto.page.html',
  styleUrls: ['./categoriaproducto.page.scss'],
})
export class CategoriaproductoPage implements OnInit {
  
  paramId: any;
  seguros: any;

  constructor(
    public api: EndpointService,
    private router: Router,
    public activatedRoute: ActivatedRoute) {
    this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    this.ListarSeguros();
  }

  ListarSeguros() {
    this.api._GetFiltroCategorias(this.paramId).subscribe(response => {
      this.seguros = response;
      console.log('filtro por aseguradora', this.seguros);
    });
  }

  detalle(id){
    console.log('id del producto', id);
    this.router.navigate(['categoriascotizar', {id:id}]);
  }

}
